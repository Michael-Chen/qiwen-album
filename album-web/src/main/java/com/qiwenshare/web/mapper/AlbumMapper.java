package com.qiwenshare.web.mapper;

import com.qiwenshare.web.domain.AlbumBean;
import com.qiwenshare.web.domain.UserBean;

import java.util.List;

public interface AlbumMapper {

    List<AlbumBean> getAlbumListByUser(UserBean userBean);
    AlbumBean selectAlbum(AlbumBean albumBean);

    int insertAlbum(AlbumBean albumBean);

    void uploadAlbum(AlbumBean albumBean);

    void deleteAlbum(AlbumBean albumBean);
}
