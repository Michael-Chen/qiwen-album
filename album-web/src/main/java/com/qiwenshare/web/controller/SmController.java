package com.qiwenshare.web.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户控制类
 *
 * @author ma116
 */
@RestController
@RequestMapping("/sm")
public class SmController {
    /**
     * @return
     */
    @RequestMapping("/systemManagement")
    @ResponseBody
    @RequiresPermissions("admin")
    public ModelAndView essayIndex() {
        ModelAndView mv = new ModelAndView("/sm/systemMgr.html");
        return mv;
    }
}
