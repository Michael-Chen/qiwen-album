package com.qiwenshare.web.domain;

import javax.persistence.*;

/**
 * 文章回复实体类
 *
 * @author ma116
 */
@Table(name = "reply")
@Entity
public class ReplyBean {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long replyId;
    @Column
    private long remarkedId;
    @Column
    private String replyDate;
    @Column
    private long replyUserId;
    @Column
    private String replyUserName;
    @Column
    private long repliedUserId;
    @Column
    private String repliedUserName;
    @Column
    private String replyContent;

    public long getReplyId() {
        return replyId;
    }

    public void setReplyId(long replyId) {
        this.replyId = replyId;
    }

    public long getRemarkedId() {
        return remarkedId;
    }

    public void setRemarkedId(long remarkedId) {
        this.remarkedId = remarkedId;
    }

    public void setReplyUserId(long replyUserId) {
        this.replyUserId = replyUserId;
    }

    public void setRepliedUserId(long repliedUserId) {
        this.repliedUserId = repliedUserId;
    }

    public String getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(String replyDate) {
        this.replyDate = replyDate;
    }


    public long getReplyUserId() {
        return replyUserId;
    }

    public long getRepliedUserId() {
        return repliedUserId;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public String getReplyUserName() {
        return replyUserName;
    }

    public void setReplyUserName(String replyUserName) {
        this.replyUserName = replyUserName;
    }

    public String getRepliedUserName() {
        return repliedUserName;
    }

    public void setRepliedUserName(String repliedUserName) {
        this.repliedUserName = repliedUserName;
    }


}
