package com.qiwenshare.web.domain;

import javax.persistence.*;

/**
 * 操作日志基础信息类
 *
 * @author ma116
 */
@Table(name = "operationlog")
@Entity
public class OperationLogBean {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long operationlogid;

    private long userid;

    private String operation;

    private String operationObj;

    private String terminal;

    private String result;

    private String detail;

    private String source;

    private String time;

    private String loglevel;

    public long getOperationlogid() {
        return operationlogid;
    }

    public void setOperationlogid(long operationlogid) {
        this.operationlogid = operationlogid;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperationObj() {
        return operationObj;
    }

    public void setOperationObj(String operationObj) {
        this.operationObj = operationObj;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLoglevel() {
        return loglevel;
    }

    public void setLoglevel(String loglevel) {
        this.loglevel = loglevel;
    }

}
