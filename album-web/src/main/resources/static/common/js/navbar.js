document.write("<link rel='stylesheet' href='/common/css/navbar.css' type='text/css'></link>" +
    "<link rel='stylesheet' href='/sm/font/iconfont.css' type='text/css' />");
    //"<script type=\"text/javascript\" charset=\"utf-8\" src=\"/common/js/copyright1.js\"></script>");



var urlObj = {};
urlObj.indexHtml = "/index";
urlObj.essayHtml = "/essay/essayIndex";
urlObj.albumHtml = "/album/albumindex";
urlObj.smHtml = "/sm/systemManagement";

urlObj.loginHtml = "/user/userLogin.html";
urlObj.registerHtml = "/user/userregister";
urlObj.chatHtml = "/chat/chatIndex";
urlObj.daHtml = "/stock/stockIndex";
urlObj.fileHtml = "/file/fileindex";

var navHtml = "<div class='layui-header'>" +
    //"<div class='layui-logo'>SCP</div>" +
    "<ul class='layui-nav lay-filter=''>" +
    "<li class='layui-nav-item'><a href='https://www.qiwenshare.com/' target='_blank'><img style='width: 160px;' src='/common/image/logo_head.png' ></img></a></li>" +
    "<li id='indexNavbarId' class='layui-nav-item'><a href='" + urlObj.indexHtml + "' id='indexNavbarId'>首页</a></li>" +
    "<li id='albumNavbarId' class='layui-nav-item'><a url='" + urlObj.albumHtml + "' id='albumNavbarId' onclick='checkLogin(this)'>相册</a></li>" +
    "<li id='fileNavbarId' class='layui-nav-item'><a href='" + urlObj.fileHtml + "' id='fileNavbarId'>文件</a></li>" +
    "</ul><ul class='layui-nav layui-layout-right'>" +
    "<li id='login-nav-item' class='layui-nav-item'><a href='" + urlObj.loginHtml + "'>登陆</span></a></li>" +
    "<li id='userNav' class='layui-nav-item'>" +
    "<a id='persionhomepageid' href=''><img id='head-nav-img' src='//t.cn/RCzsdCq' class='layui-nav-img'><span id='head-nav-name'>我</span></a><dl class='layui-nav-child'>" +
    "<dd><a id='userSettingNav' href='javascript:;'>修改信息</a></dd>" +
    "<dd><a onclick='quitLogin1()' href='javascript:;'>退出</a></dd></dl>" +
    "</li>" +
    "</ul></div>";

var bottom = " 	<nav class='navbar navbar-default'> 	  " +
    "<div class='container'> 	    " +
    "<div class='navbar-header'> 	    	<div class='col-xs-3'> 	    		" +
    "<a id='navbar-toggle' href='" + urlObj.indexHtml + "' class='navbar-toggle collapsed' style='border-color:#f8f8f8;text-align:center;'> 	    		<span class='glyphicon glyphicon-home' aria-hidden='true'></span> 	    		<p style='font-size: 10px'>首页</p> 	    		</a> 	    	</div> 	    	<div class='col-xs-3'> 	    		" +
    "<a id='navbar-toggle' href='" + urlObj.chatHtml + "' class='navbar-toggle collapsed' style='border-color:#f8f8f8; text-align:center;'> 	    		<span class='glyphicon glyphicon-comment' aria-hidden='true'></span> 	    		<p style='font-size: 10px'>消息</p> 	    		</a> 	    		 	    	</div> 	    	<div class='col-xs-3'> 	    		" +
    "<a id='navbar-toggle' href='" + urlObj.smHtml + "' class='navbar-toggle collapsed' style='border-color:#f8f8f8;text-align:center;'> 	    		<span class='glyphicon glyphicon-cog' aria-hidden='true'></span> 	    		<p style='font-size: 10px'>工作</p> 	    		</a> 	    		 	    	</div> 	    	<div class='col-xs-3'> 	    		" +
    "<a id='navbar-toggle' href='" + urlObj.loginHtml + "' name='myInfoName' class='navbar-toggle collapsed myInfoName' style='border-color:#f8f8f8;text-align:center;'> 	    		<span class='glyphicon glyphicon-user' aria-hidden='true'></span> 	    		<p style='font-size: 10px'>我</p> 	    		</a> 	    		 	    	</div> 	    </div> 	 	    <!-- Collect the nav links, forms, and other content for toggling --> 	    <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'> 	      <footer> 				<p class='pull-right'> 					" +
    "<a href='#'>回到顶部</a> 				</p> 				<p> 					&copy; 2016 Company, Inc. &middot; <a href='#'>Privacy</a> &middot; 					<a href='#'>Terms</a> 				</p> 			</footer> 	    </div> 	  </div> 	</nav>";

var goTop = "<a h ref='#'>" +
    "<div class='goToTop'>" +
    "<i class='iconfont'>&#xe61f;</i><span>回到<br/>顶部</span>" +
    "</div>" +
    "</a>";

function quitLogin1() {
    var form = document.createElement('form');
    form.action = '/logout';
    form.method = 'post';
    $(document.body).append(form);
    form.submit();

}

$(document).ready(function () {
    var divNode = document.createElement("div");
    divNode.setAttribute("id", "navBarId");
    divNode.setAttribute("class", "head");
    var first = document.body.firstElementChild;//得到页面的第一个元素 
    document.body.insertBefore(divNode, first);
    document.getElementById("navBarId").innerHTML = navHtml;



    checkUserLoginInfo();

    recordCurrentClick();
    //添加回到顶部图标及其效果
    $("body").append(goTop);
    $(".goToTop").mouseenter(function () {
        $(".goToTop .iconfont").css("display", "none");
        $(".goToTop span").css("display", "block");
        $(".goToTop").css("line-height", "20px");
    });
    $(".goToTop").mouseleave(function () {
        $(".goToTop .iconfont").css("display", "block");
        $(".goToTop span").css("display", "none");
        $(".goToTop").css("line-height", "40px");
    });
    //回到顶部图标监听事件
    $(".goToTop").on("click",function() {
        $('html,body').animate({ scrollTop: 0 }, 'fast'); 
    });

    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function () {
        var element = layui.element;

        //…
    });

    $(".layui-col-xs1").css("width", $(".layui-col-xs1").css("width"))
    $(".layui-col-xs2").css("width", $(".layui-col-xs2").css("width"))
    $(".layui-col-xs3").css("width", $(".layui-col-xs3").css("width"))
    $(".layui-col-xs4").css("width", $(".layui-col-xs4").css("width"))
    $(".layui-col-xs5").css("width", $(".layui-col-xs5").css("width"))
    $(".layui-col-xs6").css("width", $(".layui-col-xs6").css("width"))
    $(".layui-col-xs7").css("width", $(".layui-col-xs7").css("width"))
    $(".layui-col-xs8").css("width", $(".layui-col-xs8").css("width"))
    $(".layui-col-xs9").css("width", $(".layui-col-xs9").css("width"))
    $(".layui-col-xs10").css("width", $(".layui-col-xs10").css("width"))
    $(".layui-col-xs11").css("width", $(".layui-col-xs11").css("width"))
    $(".layui-col-xs12").css("width", $(".layui-col-xs12").css("width"))
});

function recordCurrentClick() {
    var currentUrl = window.location.href;
    if (currentUrl.indexOf("/album/") > 0) {
        $("#albumNavbarId").addClass("layui-this");
    }else if (currentUrl.indexOf("/file/") > 0){
        $("#fileNavbarId").addClass("layui-this");
    }else if (currentUrl.indexOf("/chat/") > 0) {

    } else if (currentUrl.indexOf("/stock/") > 0) {
        $("#learnNavbarId").addClass("layui-this");
    } else if (currentUrl.indexOf("/essay/") > 0) {
        $("#essayNavbarId").addClass("layui-this");
    } else if (currentUrl.indexOf("/sm/") > 0) {
        $("#smNavbarId").addClass("layui-this");
    } else if (currentUrl.indexOf("/user/") > 0) {

    } else {
        $("#indexNavbarId").addClass("layui-this");
    }
}


//检查用户登陆信息
function checkUserLoginInfo() {
    $.ajax({
        url: "/user/checkuserlogininfo",
        dataType: "json",
        type: "POST",
        async: false,
        success: function (result) {
            if (result.success) {
                $("#userNav").css("display", "inline-block");
                document.getElementById("login-nav-item").style.display = "none";
                var imageurl = "/common/image/imageHead.jpg";
                try {
                    imageurl = result.data.imageBeanList[result.data.imageBeanList.length - 1].imageurl;
                    if (imageurl == "" || imageurl == null) {
                        imageurl = "/common/image/imageHead.jpg";
                    }
                }catch(e){

                }

                $("#head-nav-img").attr("src", imageurl);
                $("#head-nav-img").attr("onerror", "javascript:this.src='/common/image/imageHead.jpg';");
                $("#persionhomepageid").attr("href", "/user/persionhomepage.html?userId=" + result.data.userId);
                $("#head-nav-name").text(result.data.username);
                $("#userSettingNav").attr("href", "/user/userSetting.html?userId=" + result.data.userId);

                selectNotReadCount()
                return true;

            } else {
                $("#userNav").css("display", "none");
                document.getElementById("login-nav-item").style.display = "inline-block";

                return false;
            }
        },
        error: function (result) {
            $("#userNav").css("display", "none");
            document.getElementById("login-nav-item").style.display = "inline-block";

            return false;
            ////alert("系统繁忙!");
        }
    });
}

function showUserOption() {
    document.getElementById("userOptionId").style.display = "block";
}

function hideUserOption() {
    document.getElementById("userOptionId").style.display = "none";
}
