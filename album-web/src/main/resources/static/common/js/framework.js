document.write("<link rel='icon' href='/image/logo_M.png' type='image/x-icon'/>");
/*document.write("<link rel='stylesheet'  href='/static/common/plugins/bootstrap-3.3.7/dist/css/bootstrap.css' type='text/css' />");*/
document.write("<link rel='stylesheet'  href='/common/css/scp.css' type='text/css' />");
document.write("<script type='text/javascript' charset='utf-8' src='/common/plugins/jquery/jquery-3.3.1.min.js'></script>");
/*document.write("<script type='text/javascript' charset='utf-8' src='/static/common/plugins/bootstrap-3.3.7/js/collapse.js'></script>");*/
document.write("<script type='text/javascript' charset='utf-8' src='/common/plugins/bootstrap-3.3.7/dist/js/bootstrap.js'></script>");
document.write("<script type='text/javascript' charset='utf-8' src='/common/plugins/vue/vue.js'></script>");
document.write("<script type='text/javascript' charset='utf-8' src='/common/js/common.js'></script>");
document.write("<script type='text/javascript' charset='utf-8' src='/common/js/websocket.js'></script>");
document.write("<script type='text/javascript' charset='utf-8' src='/common/js/template-web.js'></script>");
document.write("<script src=\"https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js\"></script>")
//layer插件支持
document.write("<script type='text/javascript' charset='utf-8' src='/common/plugins/layer/layer.js'></script>");
//less插件支持
less = {
    env: "development"
};
document.write("<script type='text/javascript' charset='utf-8' src='/common/plugins/less/less.js' data-env='development'></script>");

//layui插件支持
document.write("<script type='text/javascript' charset='utf-8' src='/common/plugins/layui/layui.js'></script>");
document.write("<link rel='stylesheet'  href='/common/plugins/layui/css/layui.css' type='text/css' />");

//elementui插件支持
document.write("<script type='text/javascript' charset='utf-8' src='/common/plugins/elementui/index.js'></script>");
document.write("<link rel='stylesheet'  href='/common/plugins/elementui/index.css' type='text/css' />");

function alertMsg(message) {
    layer.alert(message, {icon: 6});
}

function timestampToTime(timestamp) {
    var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    h = date.getHours();
    m = date.getMinutes();
    s = date.getSeconds();
    return h+":"+m+":"+s;
}

function timestampToDate(timestamp) {
    var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear();
    M = date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
    D = date.getDate();
    return Y+"-"+M+"-"+D;
}

function timestampToTimeDate(timestamp) {
    var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    D = date.getDate() + ' ';
    h = date.getHours() + ':';
    m = date.getMinutes() + ':';
    s = date.getSeconds();
    return Y+M+D;
}
