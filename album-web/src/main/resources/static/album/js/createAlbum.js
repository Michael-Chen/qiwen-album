function createAlbumClick() {
    var albumname = document.getElementById("albumNameId").value;
    var albumIntro = $("#albumIntrolId")[0].value;
    var albumpower = $("input[name='power']:checked").val();
    var albumData = {
        albumname: albumname,
        albumintro: albumIntro,
        albumpower: albumpower
    };
    $.ajax({
        data: albumData,
        url: "/album/addalbum",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                alert(result.data);
                window.location.href = "albumIndex.html";
            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}